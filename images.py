import numpy as np
import skimage
from skimage import io

img1 = np.load('images/car_0.npy')
img2 = np.load('images/car_1.npy')
img3 = np.load('images/car_2.npy')
imagini = [img1,img2,img3]

#print(img1.shape)

images = np.array(imagini) #vector de vectori de imagini
#print(images.shape)

#sum valorilor pixelilor tuturor imaginilor
#print("Suma pixelilor tuturor img",np.sum(images))

#sum valorilor pixelilor pt fiecare imagine
#print("Suma pixelilor pt fiecare img",np.sum(images,axis = (1,2)))

#indexul img cu suma max
#print("Indexul img cu sum max",np.argmax(np.sum(images, axis=(1,2))))

#imaginea medie

img_medie = np.mean(images,axis = 0)
io.imshow(img_medie.astype(np.uint8)) #cast pt a putea fi afisata
#io.show()

#deviatia standard a imaginilor
dev_stand = np.std(images)
#print("Deviatie standard",dev_stand)

#normalizare img(scadere img medie din img si se imparte la dev std
#print("Normalizare", (images-img_medie)/np.std(images))


#decupati fiecare img, afisati liniile[200,300], col [280,400]
for i in range(0,len(images)):
    crop= images[i,200:300,280:400]
    io.imshow(crop.astype(np.uint8))
    io.show()

